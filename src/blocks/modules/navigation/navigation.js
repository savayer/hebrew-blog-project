import $ from "jquery";
$(document).ready(function () {

    $(document).on("scroll", onScroll);

    $(' .menu__item a').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('.menu__item').removeClass('active');    

        var href =  $(this).attr('href')
        $(this).closest('li').addClass('active')
        
        $('html, body').animate({scrollTop: $(href).offset().top - 100}, 500,
            'swing', function() {
                $(document).on("scroll", onScroll);
            });
    });
})

function onScroll() {
    var scrollPos = $(this).scrollTop();

    $('.nav__menu .menu__item a').each(function () {        
        var refElement = $($(this).attr("href"));        
        if (refElement.position().top <= scrollPos+100 && refElement.position().top + refElement.height() > scrollPos) {
            $(' .menu__item').removeClass("active");
            $(this).closest('li').addClass("active");
        }
        else{
            $(this).closest('li').removeClass("active");            
        }
    });
}

$('#toggle_menu').on('click', function() {
    $(this).toggleClass('change');
    $('#toggle_menu2').toggleClass('change');    
    $('.nav__menu-mobile-container').fadeIn();    
    $('body').css('overflow', 'hidden');
})

$('#toggle_menu2').on('click', function () {
    $(this).toggleClass('change');
    $('#toggle_menu').toggleClass('change');
    $('.nav__menu-mobile-container').fadeOut();    
    $('body').css('overflow', '');
});
