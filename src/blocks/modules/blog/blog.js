let postMore = document.querySelectorAll('.post__more');

postMore.forEach(item => {
    item.addEventListener('click', e => {
        e.preventDefault();        
        item.closest('.post').querySelector('.post__alltext').classList.toggle('active');
    })
})