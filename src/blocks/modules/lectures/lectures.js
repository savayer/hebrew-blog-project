import Masonry from 'masonry-layout';
import $ from 'jquery';

let grid = document.querySelector('.lectures__masonry');

new Masonry( grid, {    
    itemSelector: '.lectures__item',
    percentPosition: true,
    originLeft: false
});

/* let items = document.querySelectorAll('.lectures__item');

items.forEach(item => {
    item.addEventListener('click', e => {
        e.preventDefault();        
        item.classList.toggle('active')
    })
}) */

$(document).on('click', function(e) {
    let item = e.target.closest('.lectures__item');
    if (item == null) {
        $('.lectures__item').removeClass('active');
    }
})

$('.lectures__item').on('click', function(e) {
    if ($(e.target).hasClass('lectures__close')) {
        return false;
    }
    $('.lectures__item').removeClass('active');    
    $(this).toggleClass('active');
})

$('.lectures__close').on('click', function() {
    $(this).closest('.lectures__item').removeClass('active');
})