import $ from 'jquery';
import 'slick-carousel';

$('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 10000,
    rtl: true,
    arrows: true,
    infinite: false,
    prevArrow: '<i class="material-icons slider__arrow slider__prev">arrow_back_ios</i>',
    nextArrow: '<i class="material-icons slider__arrow slider__next">arrow_forward_ios</i>'
})